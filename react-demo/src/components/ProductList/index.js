import React from 'react';
import PropTypes from 'prop-types';
import './ProductList.scss';
import '../Button/Button.scss'
import Button from "../Button";
import Modal from "../Modal";
import ProductItem from "../ProductItem";
import {useState} from 'react';

export default function ProductList(props) {

    const [show, setShow] = useState(false);
    const [product, setProduct] = useState(null);

    const openModal = (e) =>{
        setShow(true);
        setProduct(e.target.dataset.cart);
    }

    const closeModal = () => {
        setShow(false);
    }

    return (
        <div className='container'>
            <Modal show={show}
                   product={product}
                   onSuccessAdd={props.onSuccess}
                   onClick={closeModal}
                   onClose={closeModal}
                   textHeader={'Are you sure you want to buy this?'}
                   textBody={'Add to cart'}
            />
            {
                props.items.map
                (item =>
                    <div className='product-container'
                         key={JSON.stringify(item.id)}>

                        <Button modification='add-to-chart'
                                onClick={openModal}
                                text='Add to cart'
                                toCart={JSON.stringify(item)}/>
                        <ProductItem item={item}
                                     onClick={props.onClick}
                                     showArrow={true}
                                     isActive={props.favourites.find((elem) => elem.id === item.id)}
                                     showCross={false}
                        />
                    </div>
                )
            }
        </div>
    );
}

ProductList.propTypes = {
    items: PropTypes.array,
    onClick: PropTypes.func
}
