import React from 'react';
import { Routes ,Route } from 'react-router-dom';
import Home from "../Home";
import Favourites from "../Favourites";
import Cart from "../Cart";

export default function Main({favourites, setFavourites, products, cart, setCart, addToFavourites, addToCart}) {

    return (
        <main>
            <Routes>
                <Route exact path='/'  element={<Home
                        favourites={favourites}
                        cart={cart}
                        products={products}
                        addToFavourites={addToFavourites}
                        addToCart={addToCart}
                    />}
                />
                <Route path='/favourites' element={<Favourites
                    favourites={favourites}
                    setFavourites={setFavourites}/>}
                />
                <Route path='/cart' element={ <Cart cart={cart} setCart={setCart}/>}/>
            </Routes>
        </main>
    );
}
