import React from 'react';
import '../ProductList/ProductList.scss'
import PropTypes from "prop-types";

export default function ProductItem({item, onClick, showArrow, showCross, isActive}) {

    let styleArrow = {
        display: showArrow ? 'inline-block' : 'none'
    }
    let styleCross = {
        display: showCross ? 'inline-block' : 'none'
    }
    let activeFavourites = isActive ? 'button button-star button-star-clicked' : 'button button-star';

    return (
        <>
            <h4>{item.name}
                <button style={styleArrow} className={activeFavourites} data-product={JSON.stringify(item)}
                        onClick={onClick}>&#9733;</button>
                <button style={styleCross} className='button button-star' data-product={JSON.stringify(item)}
                        onClick={onClick}>&nbsp;&#10008;</button>
            </h4>
            <p>{item.price}</p>
            <div className='image-container'>
                <img alt='broken' className='product-image' src={item.url}/>
            </div>
            <p>Color: {item.color}</p>
            <p>Item No: {item.article}</p>
        </>
    );

}
ProductItem.propTypes = {
    item: PropTypes.object,
    onClick: PropTypes.func
}
