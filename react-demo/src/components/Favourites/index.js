import React from 'react';
import ProductItem from "../ProductItem";
// import Header from "../Header";

export default function Favourites({favourites, setFavourites}) {

    const deleteItem = (e) => {
        if (!favourites.includes(e.target.dataset.product)) {
            const filtered = favourites.filter((item) => item.id !== JSON.parse(e.target.dataset.product).id);
            setFavourites([...filtered]);
            localStorage.setItem('favourites', JSON.stringify(filtered));
        }
    }

    return (
        <div>
            <h3>This is Favourites Page</h3>
            <h4>There are {favourites.length} items in favourites</h4>
            <div className='container'>
                {
                    favourites.map
                    (item =>
                        <div className='product-container'
                             key={JSON.stringify(item.id)}>
                            <ProductItem item={item} showArrow={true} showCross={false}
                                         onClick={deleteItem}/>
                        </div>
                    )
                }
            </div>
        </div>
    );
}
