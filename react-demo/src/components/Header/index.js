import React from 'react';
import './Header.scss';
import PropTypes from "prop-types";

export default function Header({cart, favourites}) {

    return (
        <div className='header-container'>
            <div className='header-chart'>{cart} items in cart</div>
            <div className='header-favourites'>{favourites} items in favourites</div>
        </div>
    );

}

Header.propTypes = {
    chart: PropTypes.number,
    favourites: PropTypes.number
}
