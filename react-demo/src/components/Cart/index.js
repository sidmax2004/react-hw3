import React, {useState} from 'react';
import Modal from "../Modal";
import ProductItem from "../ProductItem";

export default function Cart({cart, setCart}) {

    const [show, setShow] = useState(false);
    const [product, setProduct] = useState({});

    const deleteItem = (e) => {
        let clonedCart = [...cart];
        let product = JSON.parse(e.target.dataset.cart);
        let selectedItem = clonedCart.indexOf(product);
        clonedCart.splice(selectedItem, 1);
        setCart([...clonedCart]);
        localStorage.setItem('cart', JSON.stringify(clonedCart));
    }

    const openModal = (e) => {
        setShow(true);
        setProduct(e.target.dataset.product);
    }

    const closeModal = () => {
        setShow(false);
    }

    return (
        <div>
            <h3>This is Cart Page</h3>
            <h4>There are {cart.length} items in cart</h4>
            <div className='container'>
                <Modal show={show}
                       product={product}
                       onSuccessAdd={deleteItem}
                       onClick={closeModal}
                       onClose={closeModal}
                       textBody={'Delete?'}
                       textHeader={'Are you sure you want to delete this?'}
                />
                {
                    cart.map
                    (item =>
                        <div className='product-container'
                             key={JSON.stringify(item.id)}>

                            <ProductItem item={item} showArrow={false} showCross={true}
                                         onClick={openModal}/>
                        </div>
                    )
                }
            </div>
        </div>
    );
}
