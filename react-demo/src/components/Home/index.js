import React from 'react';
import Header from "../Header";
import ProductList from "../ProductList";

export default function Home({products, favourites, cart, addToFavourites, addToCart}){

    return(
        <div>
            <Header favourites={favourites.length} cart={cart.length}/>
            <ProductList items={products} onClick={addToFavourites} onSuccess={addToCart} favourites={favourites}/>
        </div>
    );
}
